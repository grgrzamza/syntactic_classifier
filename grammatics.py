STARTSYMBOL = "S"
TERMINALS = {"a","b","c","d","e","f","g","h","i","j"}
NONTERMINALS = {"S","A","B"}
RULES = [("S",("a","A")), ("S",("b", "B")), ("A",("c", "A","d")), ("A",("f","B","i")),
("A",("e")), ("B",("c","B","f")),("B",("g")),("B",("h","j","A"))]


def valid_symbols(sequence):
    return all(symbol in TERMINALS for symbol in sequence)

def classify(sequence):
    return "Matches" if is_nonterminal(sequence, STARTSYMBOL) else "Doesn't match"

def is_nonterminal(sequence, nonterminal):
    return get_nonterminal_len(sequence, nonterminal) == len(sequence)

def get_nonterminal_len(sequence, nonterminal):    
    rules = [rule for rule in RULES if rule[0] == nonterminal]
    for rule in rules:
        index = 0
        suits = True
        for element in rule[1]:
            if element in TERMINALS:
                if index >= len(sequence) or element != sequence[index]:
                    suits = False
                    break
                else:
                    index += 1
            elif element in NONTERMINALS:
                nonterminal_len = get_nonterminal_len(sequence[index:], element)
                if nonterminal_len == 0:
                    suits = False
                    break
                else:
                    index += nonterminal_len
        if suits:
            return index
    return 0






