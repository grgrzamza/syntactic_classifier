from grammatics import classify, valid_symbols
from graphic import display_result
from sys import argv

def main():
    sequence = argv[1]
    if not valid_symbols(sequence):
        print("Wrong sequence")
        return
    print(classify(sequence))
    display_result(sequence)

main()