import matplotlib.pyplot as plt
import math

ANGLES = {"a" : 40, "b" : 75, "c" : 0, "d": -80,"e" : 20, "f" : -45,"g" : 15, "h" : 70, "i" : -50, "j" : 30}
LINELEN = 100

def get_coords_step(symbol):
    angle = ANGLES[symbol]
    return LINELEN * math.cos(math.radians(angle)), LINELEN * math.sin(math.radians(angle))

def display_result(sequence):
    x = 10
    y = 100
    for symbol in sequence:
        stepX,stepY = get_coords_step(symbol)
        plt.plot([x, x + stepX],[y, y + stepY], lw = 1)
        x += stepX
        y += stepY
    figure = plt.gcf()
    figure.canvas.set_window_title('Syntactic classifier graphic')
    plt.show()